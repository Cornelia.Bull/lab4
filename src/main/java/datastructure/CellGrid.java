package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private cellular.CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        for (int row= 0; row < this.rows; row++){
            for (int column= 0; column<this.columns; column++){
                this.grid[row][column]= initialState;
            }
        }
    }
    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;   
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid grid2 = new CellGrid(this.rows, this.columns, null);
        for (int row=0; row<this.rows; row++){
            for (int column=0; column<this.columns; column++){
                grid2.set(row, column, this.get(row, column));
            }
        }
        return grid2;
    }
    
}
